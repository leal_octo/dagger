package main

import (
	"context"
	"fmt"
	"math"
	"math/rand"
)

type DemoDagger struct{}


func (m *DemoDagger) Publish(ctx context.Context, src *Directory) (string, error) {
	_, err := m.Test(ctx, src)
	if err != nil {
		return "", err
	}
	address, err := m.BuildAppContainer(ctx, src).
    Publish(ctx, fmt.Sprintf("ttl.sh/demo-dagger-octo-%.0f", math.Floor(rand.Float64()*10000000)))

	return address, err
}

func (m *DemoDagger) BuildAppContainer(ctx context.Context, src *Directory) *Container {
	build := m.BuildApp(src)
	return dag.Container().
		From("alpine:latest").
		WithFile("/bin/greeting", build).
		WithExposedPort(4000).
		WithEntrypoint([]string{"/bin/greeting"})
}

// Build application
func (m *DemoDagger) BuildApp(src *Directory) *File {
	return dag.Container().
		From("golang:latest").
		WithMountedDirectory("/src", src).
		WithWorkdir("/src").
		WithExec([]string{"go", "build", "-tags", "netgo", "-o", "greeting"}).
		File("/src/greeting")
}

// Test application
func (m *DemoDagger) Test(ctx context.Context, src *Directory) (string, error) {
	return dag.Container().
		From("golang:latest").
		WithMountedDirectory("/src", src).
		WithWorkdir("/src").
		WithExec([]string{"go", "test"}).
		Stdout(ctx)
}

// Deployer l'infra avec Pulumi
func (m *DemoDagger) DeployInfra(ctx context.Context, src *Directory, stack string, key_id string, secret_access_key string, session_token string, pulumi_token string) (string, error){
  return dag.Container().
    From("pulumi/pulumi:latest").
    WithEnvVariable("AWS_ACCESS_KEY_ID", key_id).
    WithEnvVariable("AWS_SECRET_ACCESS_KEY", secret_access_key).
    WithEnvVariable("AWS_SESSION_TOKEN", session_token).
    WithEnvVariable("PULUMI_ACCESS_TOKEN", pulumi_token).
    WithMountedDirectory("/pulumi", src).
    WithWorkdir("/pulumi").
    WithExec([]string{"up", "--yes", "--stack", stack}).
    Stdout(ctx)
}

