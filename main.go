package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/{name}", home)

	log.Print("Starting server on port :4000")
	err := http.ListenAndServe(":4000", mux)
	log.Fatal(err)
}

func home(w http.ResponseWriter, r *http.Request) {
	name := r.PathValue("name")
	message := greeting(name)
	w.Write([]byte(message))
}

func greeting(name string) string {
	return fmt.Sprintf("Hello %s !", name)
}
