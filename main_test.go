package main

import (
	"gotest.tools/v3/assert"
	"testing"
)

func TestGreeting(t *testing.T) {
	g := greeting("Alex")
	expect := "Hello Alex !"

	assert.Equal(t, expect, g)
}
